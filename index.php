<?php 
  if( !empty( $_GET['type'] ) and !empty( $_GET['code'] ) ) {
    
    if ( 'error' == $_GET['type'] ) {
      
      if ( 1 == $_GET['code']) {

        $error = "Une erreur est survenu ! Les données que vous avez saisie ne correspondent pas aux données attendu !";

      } elseif ( 2 == $_GET['code']) {
        
        $error = "Une erreur est survenue lors de l'envois du mail, merci de réessayer ultérieurement";

      }
    
    } elseif ( 'success' == $_GET['type']) {
      
      $success = "Votre message à bien été envoyé. Nous vous répondrons dans les plus brefs délais .";
      
    }

  }
?> 


<!doctype html>
<html  lang="fr">
  <head>
      <!--metatag-->
      <meta charset="utf-8" /> <!-- Definition de l'encodage des caractères -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />

      <!-- La propriété initial-scale contrôle le niveau de zoom lorsque la page est chargée pour la première fois. -->
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="title" content="WebAgency - Agence digitale" />

      <!-- Spécification de la langue utilisée sur les pages -->
      <meta name="Content-language" content="french" />

      <!--  Spécification du type de public visé (protection éventuelle de publication pour des publics non avertis ou sensibles -->
      <meta name="Rating" content="General" />

      <!-- Spécification d'indexage pour robots -->
      <meta name="robots" content="Index" />

      <!-- Spécification de l'auteur du site -->
      <meta name="author" content="Joel Delzongle" />

      <!--  Spécification de description du contenu du site OU de la rubrique visée -->
      <meta name="description" content="WebAgency: l'agence de tous vos projets. Nous réalisons tous sortes de site web ou d'application mobile en vous accompagnant durant toutes la durée de votre projet" />
     
      <!--  Spécification des mots-clés -->
          <!-- !!! DEPRECIE !! Google n'en veut plus -->
      <!-- <meta name="keywords" content="Agence digitale, web, mobile, projet, création, portfolio, WebAgency, application, gestion de projet, design, ux-design, ui-design, interface utilisateur, expérience utilisateur, seo, référencement naturel, service "> -->

      <title>WebAgency</title>
      
        <!-- Stylesheet -->
      <link rel="stylesheet" href="css/style.css" />
      <!--  Font Awesome CDN -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
     
      <!-- Favicon -->
      <link rel="icon" type="image/png" href="images/logo.png" />
      <!-- Pour IE -->
      <link rel="shortcut icon" type="image/x-icon" href="images/logo.png" />

      <link rel="author" href="https://plus.google.com/104956904708953723784" />
      <link rel="publisher" href="https://plus.google.com/104956904708953723784" /> 

      <!-- Open Graph FACEBOOK -->
      <meta property="og:title" content="Webagency - Votre agence digitale" />
      <meta property="og:type" content="website" />
      <meta property="og:url" content="td.html" />
      <meta property="og:description" content="La WebAgency vous accompagnent dans tous vos projets web ou mobile." />
      <meta property="og:image:width" content="26 1" />
      <meta property="og:image:height" content="57" />
      <meta property="og:image" content="images/logo.png" />

      <!-- TWITTER Summary Card Meta -->
      <meta name="”twitter:card”" content="summary" />
      <meta name="”twitter:site”" content="@webagency" />
      <meta name="”twitter:title”" content="Webagency" />
      <meta name="”twitter:description”" content="La WebAgency vous accompagnent dans tous vos projets web ou mobile." />
      <meta name="”twitter:image”" content="images/logo.png" /> 
  </head>
  <body class="">
    <main>
      <!-- #####################################  -->
      <!-- ========> Section HEADER <=========  -->
      <!-- #####################################  -->
      <header id="header" class="main-header">
        <div class="header-inner">
        	<div class="header-logo">
        		<img id="logo" src="images/logo.png" title="Logo de la WebAgency" alt="Logo de la WebAgency" />
        	</div>
          <!-- Hamburger button -->
          <a id="hamburger-link" class="">
            <div class="h-bar"></div>
            <div class="h-bar"></div>
            <div class="h-bar"></div>
          </a>
          <!-- Main Navigation Bar -->
        	<nav class="header-nav">
            <!-- Main navbar -->
            <ul id="nav-bar">
              <li class="nav-item active"><a class="nav-link " href="#slider" title="Accueil">Accueil</a></li><!--
            --><li class="nav-item "><a class="nav-link " href="#anchor-services" title="Services">Sevices</a></li><!--
            --><li class="nav-item"><a class="nav-link" href="#anchor-portfolio" title="Portfolio">Portfolio</a></li><!--
            --><li class="nav-item"><a class="nav-link" href="#anchor-contact" title="Contact">Contact</a></li>
          	</ul>
        	</nav>
        </div><!-- /.header-inner -->
      </header> 
      <!-- #####################################  -->
      <!-- ========> Section SLIDER <=========  -->
      <!-- #####################################  -->
      <section id="top-section">
        <div id="slider" class="">
          <!-- Slider Image container -->
          <figure>
            <img class="img-slide" title="Image slider 1" src="images/slider/bg1.jpg" alt="Une petite fille avec les doigts remplis de peinture de plusieurs couleurs" />
            <img class="img-slide" title="Image slider 2" src="images/slider/bg2.jpg" alt="Un petit garçon qui parle dans un porte voix." />
            <img class="img-slide" title="Image slider 1" src="images/slider/bg1.jpg" alt="Une petite fille avec les doigts remplis de peinture de plusieurs couleurs"  />
          </figure>	
          <!-- Slider text content for DESKTOP  -->
          <div id="header-text-desktop" class="header-text-style">
            <h1><span>webagency:</span> l'agence de tous <br /> vos projet !</h1>
            <p>Vous avez un projet web ? La WebAgency vous aide à le réaliser !</p>
            <!-- Form with redirect button to services section -->
            <form action="#anchor-services">
              <input id="btn-slider" type="submit" value="Plus d'infos" />
            </form>
          </div>				 
            <!-- Slider left controle  -->
          <div id="control-left">
            <i class="fa fa-angle-left fa-3x" aria-hidden="true"></i>
          </div>
            		<!-- Slider right controle  -->
          <div id="control-right">
            <i class="fa fa-angle-right fa-3x"  aria-hidden="true"></i>
          </div>
        </div>

        <!--  TimeLine -->
        <span id="timeline"><!-- Animation représentant le défilement du temps --></span>

        <!-- Header text for Mobile -->
        <div id="header-text-mobile" class="header-text-style">
          <h1><span>webagency:</span> l'agence de tous<br /> vos projet !</h1>
          <p>Vous avez un projet web ? La WebAgency vous aide à le réaliser !</p>
          <form action="#anchor-services">
            <input id="btn-slide-2" type="submit" value="Plus d'infos" />
          </form>
        </div>

        <!-- ANCHOR SERVICES -->
        <div id="anchor-services"></div>  

        <!-- White block -->
        <div id="white-block-services" class="blank"></div>



        <!-- Form Error and Success Message -->
        <div id="retour-form">
          <?php 
            if ( !empty($error) ) {
              echo '<p id="warn">' . $error . '</p>';
            } 
            if ( !empty($success) ) {
              echo '<p id="succ">' . $success . '</p>';
            } 
          ?>
        </div> 
      </section>


      <!-- #####################################  -->
      <!-- ========> Section SERVICES <=========  -->
      <!-- #####################################  -->
      <section id="services" class="section">  
        <h2>nos services</h2>  
        <!-- Line HR with blue circle -->
        <div class="subtitle-element">          
          <hr class="subtitle-line subtitle-line-left" />
          <div class="fa-circle-element">
            <i class="fa fa-circle" aria-hidden="true"></i>
          </div>
          <hr class="subtitle-line subtitle-line-right" />
        </div>
        <!-- Subtitle -->
        <p  class="subtitle">
          Nous réalisons tous types de projet web ou mobile. Nous vous accompagnons durant toute la durée de votre projet.
        </p>
        <!-- Content Block of services section -->
        <div id="services-content"> 
          <!-- Left Block ->  Mac Book Image -->
          <div id="services-left-block">
            <img src="images/img-services.png" title="Ordinateur Apple Mac Book" alt="Ecran d'ordinateur affichant un site web" width="510" height="319" />
          </div>
          <!-- Right Blok -->
          <div id="services-right-block">
            <div id="ux-design" class="services-items" title="Icône UX-design">
              <!-- Icone UX Design -->
              <span class="fa-stack fa-2x">
                <i class="fa fa-circle-thin fa-stack-2x" aria-hidden="true"></i>
                <i class="fa fa-line-chart fa-stack-1x" aria-hidden="true"></i>
                <i class="fa fa-circle" aria-hidden="true"></i>
              </span>
              <div class="services-items-content">
                <h3>Expérience utilisateur (UX design)</h3>
                <p>L’UX design est une discipline qui prend en compte et anticipe les attentes et les besoins de l’utilisateur pour créer un site web ou une application. </p>
              </div>
            </div>
            <div id="ui-design" class="services-items" title="Icône UI-design">
              <!-- Icone UI Design -->
              <span class="fa-stack fa-2x">
                <i class="fa fa-circle-thin fa-stack-2x" aria-hidden="true"></i>
                <i class="fa fa-cubes fa-stack-1x" aria-hidden="true"></i>
                <i class="fa fa-circle" aria-hidden="true"></i>
              </span>
              <div class="services-items-content">
                <h3>Interface utilisateur (UI design)</h3>
                <p>L'UI design correspond a la facilité que l’utilisateur accède aux différentes informations présentes sur un site web. </p>
              </div>
            </div>
            <div id="seo" class="services-items" title="Icône SEO">
              <!-- Icone UX Design -->
              <span class="fa-stack fa-2x">
                <i class="fa fa-circle-thin fa-stack-2x" aria-hidden="true"></i>
                <i class="fa fa-pie-chart fa-stack-1x" aria-hidden="true"></i>
                <i class="fa fa-circle " aria-hidden="true"></i>
              </span>
              <div class="services-items-content">
                <h3>Référencement (<span id="seo-uppercase">seo</span>)</h3>
                <p>L’optimisation pour les moteurs de recherche, référencement naturel ou SEO, est un ensemble de techniques pour optimiser la visibilité d'une page web dans les pages de résultats de recherche . </p>
              </div>
            </div>
          </div>
        </div>
        <!-- White block -->
        <div class="blank"></div>
      </section>

      <!-- ANCHOR PROTFOLIO -->
      <div id="anchor-portfolio"></div>
      
      
      <!-- #######################################  -->
      <!-- =========> Section PORTFOLIO <=========  -->
      <!-- #######################################  -->
      <section id="portfolio" class="section">
        <h2>nos projets</h2>
        <!-- Line HR with blue circle -->
        <div class="subtitle-element">          
          <hr class="subtitle-line subtitle-line-left" />
          <div class="fa-circle-element">
            <i class="fa fa-circle" aria-hidden="true"></i>
          </div>
          <hr class="subtitle-line subtitle-line-right" />
        </div>
        <!-- Subtitle -->
        <p class="subtitle">
          Vous trouverez ci-dessous l'ensemble de nos meilleurs travaux. Grâce auxquels vous pourrez juger de notre professionalisme.
        </p>
        <!--Portfolio navbar-->
        <nav>
          <ul id="portfolio-navbar">
            <li class="active portfolio-nav-item">
              <a class="portfolio-nav-link active" href="#">
                Tous<i class="fa fa-caret-down fa-3x caret"></i>
              </a>
            </li>
            <li class="portfolio-nav-item">
              <a class="portfolio-nav-link" href="#">
                Création<i class="fa fa-caret-down fa-3x caret"></i>
              </a>
            </li>
            <li class="portfolio-nav-item">
              <a class="portfolio-nav-link" href="#">
                Corporate<i class="fa fa-caret-down fa-3x caret"></i>
              </a>
            </li>
            <li class="portfolio-nav-item">
              <a class="portfolio-nav-link" href="#">
                Portfolio<i class="fa fa-caret-down fa-3x caret"></i>
              </a>
            </li>
          </ul>
        </nav>
        <!-- Portfolio -->
        <div class="portfolio-project">
          <!-- Portfolio First line -->
          <ul id="portfolio-first-line" class="portfolio-line">
            <li class="portfolio-item">
              <a href="#" class="project-link">
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/01.jpg" title="Site web professionnel - Irma et sa boule de crystal" alt="Image représentant le site web professionnel de Irma et sa boule de crystal" />
                  <figcaption class="project-description">
                    <h3>Site web professionnel</h3>
                    <p>Irma et sa boule de crystal</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link">
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/02.jpg" title="Site web pour enfants - Le lapin d'Oz" alt="Image représentant le projet numéro 2" />
                  <figcaption class="project-description">
                    <h3>Site web pour enfants</h3>
                    <p>Le lapin d'Oz</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link">
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/03.jpg" title="Site web académie de théâtre - Pompadour Academy" alt="Image représentant le Site web de l'académie de théâtre Pompadour Academy" />
                  <figcaption class="project-description">
                    <h3>Site web académie de théâtre</h3>
                    <p>Pompadour Academy</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link">
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/04.jpg" title="Portfolio - Alain Lacroix WebDesigner" alt="Image représentant le portfolio de Alain Lacroix WebDesigner" />
                  <figcaption class="project-description">
                    <h3>Portfolio</h3>
                    <p>Alain Lacroix WebDesigner</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link"> 
                <figure class="fig-project">  
                  <img class="img-project" src="images/portfolio/05.jpg" title="Site e-commerce - Sacs en folie" alt="Image représentant le site e-commerce Sacs en folie" />
                  <figcaption class="project-description">
                    <h3>Site e-commerce</h3>
                    <p>Sacs en folie</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link"> 
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/06.jpg" title="Site Agence de tourisme - 1001 destinations" alt="Image représentant le site de l'agence de tourisme 1001 destinations" />
                  <figcaption class="project-description">
                    <h3>Site Agence de tourisme</h3>
                    <p>1001 destinations</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link">     
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/07.jpg" title="Site web d'entreprise - Jet Fusion" alt="Image représentant le Site web de l'entreprise Jet Fusion" />
                  <figcaption class="project-description">
                    <h3>Site web d'entreprise</h3>
                    <p>Jet Fusion</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
            <li class="portfolio-item">
              <a href="#" class="project-link">     
                <figure class="fig-project">
                  <img class="img-project" src="images/portfolio/08.jpg" title="Blog - L'histoire de Julie Rose" alt="Image représentant le blog de Julie Rose" />
                  <figcaption class="project-description">
                    <h3>Blog</h3>
                    <p>L'histoire de Julie Rose</p>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </figcaption>
                </figure>
              </a>
            </li>
          </ul>
        </div>        
      </section>

      
      <!-- #####################################  -->
      <!-- ==========> Section CONTACT <========  -->
      <!-- #####################################  -->
    
      <section id="contact" class="section">
        <div id="anchor-contact"></div>
        <!--  Background map DESKTOP -->
        <div id="bg-map" class="map">
          <img id="img-map" src="../images/map.png" alt="Carte représentant la position de l'agence WebAgency" title="Position de l'agence WebAgency">
            <!-- Contact form-->
            <form id="contact-form" class="form-contact" action="php/gestion-form-contact.php" method="post">
              <div id="address">
                <h2>Contact Info</h2>
                <!-- Adress -->
                <address>
                  WebAgency S.A.S
                  <br /> 25, Rue d'Hauteville, 75010 Paris
                  <br /> Tél: 01.02.03.04.05
                </address>
              </div>
              <!--Input field Name -->
              <div class="form-group">
                <label for="name"></label>
                <input id="name" type="text" name="name" placeholder="Votre nom" required />
              </div>
              <!--Input field Email-->
              <div class="form-group">
                <label for="email"></label>
                <input id="email" type="email" name="email" placeholder="Votre e-mail" required />
              </div>
              <!--Input field Subject -->
              <div class="form-group">
                <label for="subject"></label>
                <input id="subject" type="text" name="subject" placeholder="Sujet" required />
              </div>
              <!--Textarea field Message-->
              <div class="form-group">
                <textarea id="message" name="message" placeholder="Inscrivez votre message ici" required></textarea>
              </div>
              <!--Submit button-->
              <button id="form-btn-submit" type="submit">Envoyer</button>
            </form>
        </div>
        <div id="bg-map-2" class="map"></div>
      </section>
      <footer id="footer">
        <p >Copyright 2018 - Delzongle Joel</p>
      </footer>
    </main>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://raw.github.com/LeaVerou/prefixfree/master/prefixfree.min.js"></script>
    
    <script src="js/message_form.js"></script>
    <script src="js/toggle-mobile-menu.js"></script>
  </body>
</html>
