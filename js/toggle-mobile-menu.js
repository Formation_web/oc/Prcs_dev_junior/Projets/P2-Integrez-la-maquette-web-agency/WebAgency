$(document).ready(function(e) {

    $("#hamburger-link").click(function(e) {
       e.preventDefault();

        // Ajour de la classe qui transforme le burger en croix rouge        
        $("#hamburger-link").toggleClass('selected-hamburger');
        
        $("body").toggleClass('navOpen');

        // Slide Up et Down du menu
        // $("nav.header-nav li").slideToggle('slow');

    });

    var navLink = document.getElementsByClassName("nav-link");

    $(navLink).click(function(e) {
        $("body").toggleClass('navOpen');
        $("#hamburger-link").toggleClass('selected-hamburger');
    })

});